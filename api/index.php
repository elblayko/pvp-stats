<?php

ini_set('display_errors', 1);
error_reporting(E_ALL);

/**
 *  PvP Stats Server-side API controller.
 */

// Slim Framework.
require_once 'Slim/Slim.php';
\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

// Memcached.
$mem = new Memcached();
$mem->addServer('127.0.0.1', 11211);

// MySQL.
try {
    $db = new PDO('mysql:host=localhost;dbname=pvp_stats', 'web', 'DZLf49U5nAW9ycLm');
}
catch (PDOException $e)  {
    http_response_code(500);
    header('Content-Type: application/json');
    echo json_encode(array('status' => 'error', 'msg' => 'Unable to connect to database.'));
    return;
}

$app->get('/rbg/values', function() use ($app) {
    global $mem;
    global $db;

    $query = "SELECT COUNT(*) FROM rbg";
    $result = $db->query($query);

    if ($result->fetchColumn() == 0) {
        $app->response->headers->set('Content-Type', 'application/json');
        $app->response->setStatus(204);
        return;
    }

    $query = "SELECT *
        FROM    rbg
        WHERE   date
        BETWEEN (CURDATE() - INTERVAL 7 DAY)
        AND CURDATE()";

    $md5 = md5($query);

    if ($result = ($mem->get($md5))) {
        $app->response->headers->set('Content-Type', 'application/json');
        $app->response->setStatus(200);
        $app->response->setBody($result);
        return;
    }

    $result = $db->query($query);
    $result->setFetchMode(PDO::FETCH_ASSOC);    
    
    $json = json_encode($result->fetchAll());
    $mem->set($md5, $json);
    
    $app->response->headers->set('Content-Type', 'application/json');
    $app->response->setStatus(200);
    $app->response->setBody($json);
});

/**
 *  HTTP Method:        GET
 *  Resource:           api/2v2
 *
 *  Description:        Returns data about the 2v2 bracket.
 */

$app->get('/2v2', function() use ($app) {

    /**
     *  Retrieve data from memcached if available.
     */

    global $mem;
    $ladder = $mem->get('2v2');

    /**
     *  Check that we have data from memcached, and
     *  if not, fetch it from the armory API.
     */

    if ($ladder === false) {
        $remoteContents = file_get_contents('http://us.battle.net/api/wow/leaderboard/2v2');
        if ($remoteContents === false) {
            $app->setStatus(500);
            $app->response->setBody(json_encode(array(
                'status' => 'error',
                'msg'    => 'Could not connect to Battle.net armory.'
            )));
            return;
        }
        $ladder = preg_replace('/\s+/', '', utf8_decode($remoteContents));  // Compress data.
        $mem->set('2v2', $ladder, 60*60*6);
    }

    /**
     *  All is good, send the response to the client.
     */

    $app->response->headers->set('Content-Type', 'application/json');
    $app->response->setStatus(200);
    $app->response->setBody($ladder);
});


/**
 *  HTTP Method:        GET
 *  Resource:           api/3v3
 *
 *  Description:        Returns data about the 3v3 bracket.
 */

$app->get('/3v3', function() use ($app) {

    /**
     *  Retrieve data from memcached if available.
     */

     global $mem;
    $ladder = $mem->get('3v3');

    /**
     *  Check that we have data from memcached, and
     *  if not, fetch it from the armory API.
     */

    if ($ladder === false) {
        $remoteContents = file_get_contents('http://us.battle.net/api/wow/leaderboard/3v3');
        if ($remoteContents === false) {
            $app->setStatus(500);
            $app->response->setBody(json_encode(array(
                'status' => 'error',
                'msg'    => 'Could not connect to Battle.net armory.'
            )));
            return;
        }
        $ladder = preg_replace('/\s+/', '', utf8_decode($remoteContents));  // Compress data.
        $mem->set('3v3', $ladder, 60*60*6);
    }

    /**
     *  All is good, send the response to the client.
     */

    $app->response->headers->set('Content-Type', 'application/json');
    $app->response->setStatus(200);
    $app->response->setBody($ladder);
});


/**
 *  Method:         GET
 *  Resource:       api/5v5
 *
 *  Description:    Returns data about the 5v5 bracket.
 */

$app->get('/5v5', function() use ($app) {

    /**
     *  Retrieve data from memcached if available.
     */

    global $mem;
    $ladder = $mem->get('5v5');

    /**
     *  Check that we have data from memcached, and
     *  if not, fetch it from the armory API.
     */

    if ($ladder === false) {
        $remoteContents = file_get_contents('http://us.battle.net/api/wow/leaderboard/5v5');
        if ($remoteContents === false) {
            $app->setStatus(500);
            $app->response->setBody(json_encode(array(
                'status' => 'error',
                'msg'    => 'Could not connect to Battle.net armory.'
            )));
            return;
        }
        $ladder = preg_replace('/\s+/', '', utf8_decode($remoteContents));  // Compress data.
        $mem->set('5v5', $ladder, 60*60*6);
    }

    /**
     *  All is good, send the response to the client.
     */

    $app->response->headers->set('Content-Type', 'application/json');
    $app->response->setStatus(200);
    $app->response->setBody($ladder);
});


/**
 *  Method:         GET
 *  Resource:       api/rbg
 *
 *  Description:    Returns data about the rbg bracket.
 */

$app->get('/rbg', function() use ($app) {

    /**
     *  Retrieve data from memcached if available.
     */

    global $mem;    
    $ladder = $mem->get('rbg');

    /**
     *  Check that we have data from memcached, and
     *  if not, fetch it from the armory API.
     */

    if ($ladder === false) {
        $remoteContents = file_get_contents('http://us.battle.net/api/wow/leaderboard/rbg');
        if ($remoteContents === false) {
            $app->setStatus(500);
            $app->response->setBody(json_encode(array(
                'status' => 'error',
                'msg'    => 'Could not connect to Battle.net armory.'
            )));
            return;
        }
        $ladder = preg_replace('/\s+/', '', utf8_decode($remoteContents));  // Compress data.
        $mem->set('rbg', $ladder, 60*60*6);
    }
    
    /**
     *  Send data to database for logging.
     */
     
    global $db;
    
    $array = json_decode($ladder);
    
    $query = "INSERT INTO rbg (faction, race, class, spec, date) 
            VALUES (:faction, :race, :class, :spec, CURDATE())";    
    

    /**
     *  All is good, send the response to the client.
     */

    $app->response->headers->set('Content-Type', 'application/json');
    $app->response->setStatus(200);
    $app->response->setBody($ladder);
});

$app->run();