/**
 *  Application main entry point.
 */
 
var app = angular.module('myApp', ['tc.chartjs']);

/**
 *  Graph Controller.
 */

app.controller('GraphCtrl', function($scope, BattleNetRestService, GraphingService) {
    var async = BattleNetRestService.get('rbg');
    
    async.then(function(data, status, headers, config) {
        var obj = GraphingService.composeGraphingObject('faction', data);
        $scope.data_faction = obj;
        $scope.options_faction = { responsive: true };
        
        var obj = GraphingService.composeGraphingObject('class', data);
        $scope.data_class = obj;
        $scope.options_class = { responsive: true };
    });
});