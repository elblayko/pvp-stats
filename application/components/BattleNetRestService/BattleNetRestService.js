app.service('BattleNetRestService', ['$http', '$q', function($http, $q) {
    this.get = function(resource) {
        var deferred = $q.defer();
        var http = $http.get('api/' + resource);

        http.success(function(data, status, headers, config) {
            deferred.resolve(data);
        });

        http.error(function(data, status, headers, config) {
            deferred.reject(status);
        });

        return deferred.promise;
    };
}]);