var color = {
    DK: '#C41F3B',
    DRUID: '#FF7D0A',
    HUNTER: '#ABD473',
    MAGE: '#69CCF0',
    MONK: '#00FF96',
    PALADIN: '#F58CBA',
    PRIEST: '#EEEEEE',
    ROGUE: '#FFF569',
    SHAMAN: '#0070DE',
    WARLOCK: '#9482C9',
    WARRIOR: '#C79C6E',
};

app.service('GraphingService', function() {
    this.composeGraphingObject = function(options, data) {
        var rows = data.rows;

        switch (options) {

            case 'faction': {
                var alliance = 0, horde = 0;
                rows.forEach(function(element, index, array) {
                    if (element.factionId == 0) { alliance++; }
                    else if (element.factionId == 1) { horde++; }
                });

                return [
                    {
                        value: alliance,
                        color: color.SHAMAN,
                        highlight: color.SHAMAN,
                        label: 'Alliance'
                    },
                    {
                        value: horde,
                        color: color.DK,
                        highlight: color.DK,
                        label: 'Horde'
                    }
                ];
                break;
            }

            case 'class': {
                var warrior = 0, paladin = 0, hunter = 0, rogue = 0, priest = 0, dk = 0, shaman = 0, mage = 0, warlock = 0, monk = 0, druid = 0;
                rows.forEach(function(element, index, array) {
                    if (element.classId == 1) { warrior++ }
                    else if (element.classId == 2) { paladin++ }
                    else if (element.classId == 3) { hunter++ }
                    else if (element.classId == 4) { rogue++ }
                    else if (element.classId == 5) { priest++ }
                    else if (element.classId == 6) { dk++ }
                    else if (element.classId == 7) { shaman++ }
                    else if (element.classId == 8) { mage++ }
                    else if (element.classId == 9) { warlock++ }
                    else if (element.classId == 10) { monk++ }
                    else if (element.classId == 11) { druid++ }
                });

                return [
                    {
                        value: warrior,
                        color: color.WARRIOR,
                        highlight: color.WARRIOR,
                        label: 'Warrior'
                    },
                    {
                        value: paladin,
                        color: color.PALADIN,
                        highlight: color.PALADIN,
                        label: 'Paladin'
                    },
                    {
                        value: hunter,
                        color: color.HUNTER,
                        highlight: color.HUNTER,
                        label: 'Hunter'
                    },
                    {
                        value: rogue,
                        color: color.ROGUE,
                        highlight: color.ROGUE,
                        label: 'Rogue'
                    },
                    {
                        value: priest,
                        color: color.PRIEST,
                        highlight: color.PRIEST,
                        label: 'Priest'
                    },
                    {
                        value: dk,
                        color: color.DK,
                        highlight: color.DK,
                        label: 'Death Knight'
                    },
                    {
                        value: shaman,
                        color: color.SHAMAN,
                        highlight: color.SHAMAN,
                        label: 'Shaman'
                    },
                    {
                        value: warlock,
                        color: color.WARLOCK,
                        highlight: color.WARLOCK,
                        label: 'Warlock'
                    },
                    {
                        value: monk,
                        color: color.MONK,
                        highlight: color.MONK,
                        label: 'Monk'
                    },
                    {
                        value: druid,
                        color: color.DRUID,
                        highlight: color.DRUID,
                        label: 'Druid'
                    },
                    {
                        value: mage,
                        color: color.MAGE,
                        highlight: color.MAGE,
                        label: 'Mage'
                    }
                ];
                break;
            }
        };
    };
});